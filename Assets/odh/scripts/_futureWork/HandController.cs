﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;

using UnityEngine;


#if WINDOWS_UWP
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.UI.Xaml;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Storage.Streams;
#endif


public class HandController : MonoBehaviour {


    public bool isConnected;
    //private List<MotionCaptureUnit> MCUs;

    private void Awake()
    {
        //MCUs = new List<MotionCaptureUnit>();
        
    }

    void Start()
    {
        isConnected = false;
        

#if WINDOWS_UWP
            deviceList.Clear();

            BleWatcher = new BluetoothLEAdvertisementWatcher
            { ScanningMode = BluetoothLEScanningMode.Active };

            BleWatcher.Received += Watcher_Received;

            deviceWatcher = DeviceInformation.CreateWatcher(
                "System.ItemNameDisplay:~~\"Shoulder\"",
                new string[] {
                "System.Devices.Aep.DeviceAddress",
                "System.Devices.Aep.IsConnected" },
                DeviceInformationKind.AssociationEndpoint);
            deviceWatcher.Added += DeviceWatcher_Added;
            deviceWatcher.Updated += DeviceWatcher_Updated;
            deviceWatcher.Removed += DeviceWatcher_Removed;

            deviceWatcher.Start();
            BleWatcher.Start();

            
#endif
    }

    // Update is called once per frame
    void Update () {
		
	}


#if WINDOWS_UWP


    private void Watcher_Stopped(BluetoothLEAdvertisementWatcher watcher, BluetoothLEAdvertisementWatcherStoppedEventArgs eventArgs)
        {
            Debug.WriteLine($"Watcher stopped, check that bluetooth is switched on.");
        }

        private void Watcher_Received(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args)
        {
            ulong btAddress = args.BluetoothAddress;

            if (device == null)
            {
                Task devChk = getDevice(btAddress).ContinueWith((antecedant) =>
               {
                   if (antecedant.Status == TaskStatus.RanToCompletion)
                   {
                       if (deviceList.Contains(antecedant.Result.Name))
                       {
                           device = antecedant.Result;
                           short sigStr = args.RawSignalStrengthInDBm;

                           if (device != null)
                           {
                                MotionCaptureUnit mcu = new MotionCaptureUnit();
                                mcu.Id = System.Guid.NewGuid();
                                mcu.Name = device.Name;
                                //MCUs.Add(mcu);
                                mcu.Device.ConnectionStatusChanged += deviceConnChanged;
                                FindDevice();
                           }
                       }
                   }
               });
                devChk.Wait();
            }
        }


    private async Task<BluetoothLEDevice> getDevice(ulong btAddr)
    {
        return await BluetoothLEDevice.FromBluetoothAddressAsync(btAddr);
    }

    private async Task<GattDeviceService> getService(BluetoothLEDevice dev, Guid servGuid)
    {
        gatt = await device.GetGattServicesAsync();
        if (gatt == null)
        { return null; }
        
        int serviceCount = gatt.Services.Count;
        if (serviceCount > 0)
        {
            GattDeviceService myDataService = myServices.Single(s => s.Uuid == servGuid);
            return myDataService;
        }
        else
        {
            Debug.WriteLine($"Service not found, please check identifier");
            return null;
        }
    }

    private Task<GattCharacteristic> getCharac(GattDeviceService gds, Guid charGuid)
    {
        var tcs = new TaskCompletionSource<GattCharacteristic>();
        Task.Run(async () =>
        {
            await gds.Characteristics.Single(c => c.Uuid == charGuid)
        });
        return tcs.Task;
    }

    public static Task<GattReadResult> Read(GattCharacteristic characteristic)
    {
        var tcs = new TaskCompletionSource<GattReadResult>();
        Task.Run(async () =>
        {
            var _myres = await characteristic.ReadValueAsync(BluetoothCacheMode.Cached);
            tcs.SetResult(_myres);
        });
        return tcs.Task;
    }


        
    public static Task<GattCommunicationStatus> Write(GattCharacteristic charac, byte[] msg)
    {
        var tcs = new TaskCompletionSource<GattCommunicationStatus>();
        Task.Run(async () =>
        {
            await charac.WriteValueAsync(msg.AsBuffer());
        });

        return tcs.Task;
    }

    public static Task<GattCommunicationStatus> Notify(GattCharacteristic charac, GattClientCharacteristicConfigurationDescriptorValue value)
    {
        var tcs = new TaskCompletionSource<GattCommunicationStatus>();
        Task.Run(async () =>
        {
            var _myres = await charac.WriteClientCharacteristicConfigurationDescriptorAsync(value);
            tcs.SetResult(_myres);
        });
        return tcs.Task;

    }



    #region Data Handling

        private async void StopDataStreaming()
        {
            if (IMUdataChar != null)
            {
                await enableNotifications(IMUdataChar, _unnotify);
                dispatcherTimer.Stop();
            }
        }

        private async void StreamSensorHubData()
        {
            Guid HubTx = new Guid("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");

            // Subscribe to Hub Data Characteristic

            IMUdataChar = myCharacs.Single(c => c.Uuid == HubTx);
            if (IMUdataChar.Service != null)
            {
                await enableNotifications(IMUdataChar, _notify);
                IMUdataChar.ValueChanged += GotIMUData;
                dispatcherTimer.Start();
            }
        }

        private void GotIMUData(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            byte[] newData = readFromBuffer(args.CharacteristicValue);
            currentIMUData = new float[newData.Length / 4];
            System.Buffer.BlockCopy(newData, 0, currentIMUData, 0, newData.Length);

            _txtUpdate = string.Join(Environment.NewLine, currentIMUData);
            UiUpdate();
        }

        private byte[] readFromBuffer(IBuffer data)
        {
            DataReader _reader = DataReader.FromBuffer(data);
            byte[] fileContent = new byte[_reader.UnconsumedBufferLength];
            _reader.ReadBytes(fileContent);
            return fileContent;
        }

        public static String ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
            { hex.Append(b + " "); }
            return hex.ToString();
        }


    #endregion Data Handling


#endif


}
