using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MotionCaptureUnit {
	
    public System.Guid Id { get; set; }
    public string Name { get; set; }

    public bool IsConnected { get; set; }
    public bool IsReady { get; set; }

	#if WINDOWS_UWP

        // bluetooth low energy device
        public BluetoothLEDevice Device { get; set; }

        // objects for connection
        public GattDeviceService controlService { get; set; }

        // objects for interrogation
        public GattDeviceService imuService { get; set; }
        public GattCharacteristic imuCharac { get; set; }

        // BT Stati and other configuration variables
        public BluetoothConnectionStatus devConnStat { get; set; }
        public GattCommunicationStatus CharNotStat { get; set; }
        public GattCommunicationStatus[] emgConnStat { get; set; }

	#endif

}
