using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace CareBand
{
    public class firebaseinit : MonoBehaviour
    {
        private String userId;
        public string session_id = "newSession";
		public bool connected = false;
		public bool signedIn = false;
        public bool hasNewData { get; set; }

		public DataSnapshot refmoves;
        private DataSnapshot currentMovement;
        public List<string> StoredMovements;
        public List<Quaternion> upperDataReceived = new List<Quaternion>();
        public List<Quaternion> lowerDataReceived = new List<Quaternion>();
        public GameObject PlayButton;
        private Quaternion[] tempUpper;
        private Quaternion[] tempLower;


        private FirebaseApp app;
		private Firebase.Auth.FirebaseAuth auth;
		private Firebase.Auth.FirebaseUser user;

        DatabaseReference refBandRecording;
        DatabaseReference refBandCapturing;
        //DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

        // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        void Start()
        {
            session_id = "session " + DateTime.Now.ToString("u");
            Debug.Log("firebaseinit: start()");
            hasNewData = false;
            PlayButton.SetActive(false);

            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {

                var dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available) {
                    InitializeFirebase();
					Debug.Log("FirebaseInit: done calling InitializeFirebase()");
                    StoredMovements = new List<string>();
                    connected = true;
                } else {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }

            });

        } // Start

        // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        protected virtual void InitializeFirebase()
        {

			Debug.Log("FirebaseInit: InitializeFirebase()");

			app = FirebaseApp.DefaultInstance;
			app.SetEditorDatabaseUrl("https://augur-21c69.firebaseio.com/");

			auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
			auth.StateChanged += AuthStateChanged;
			AuthStateChanged(this, null);

			/* // uncomment this block to use a service account for testing from the editor (service account prevents dummy data persistence?)
			#if UNITY_EDITOR
            	Debug.Log("FirebaseInit: Unity Editor");
            	app.SetEditorP12FileName("augur-21c69-e29a74ad95ca.p12");
            	app.SetEditorServiceAccountEmail("odh-tester@augur-21c69.iam.gserviceaccount.com");
            	app.SetEditorP12Password("notasecret");

			#elif UNITY_IOS
				Debug.Log("FirebaseInit: Iphone");

			#elif UNITY_ANDROID
				Debug.Log("FirebaseInit: Android");

			#endif
			*/

			Debug.Log("FirebaseInit: Logging in with wild@brookes.ac.uk");
			auth.SignInWithEmailAndPasswordAsync("wild@brookes.ac.uk", "nosecretatall123").ContinueWith(task => {

				if (task.IsCanceled) {
					Debug.LogError("FirebaseInit: InitializeFirebase(): SignInWithEmailAndPasswordAsync was canceled.");
					return;
				}

				if (task.IsFaulted) {
					Debug.LogError("FirebaseInit: InitializeFirebase(): SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
					return;
				}

				user = task.Result;
				Debug.LogFormat("FirebaseInit: InitializeFirebase(): User signed in successfully: {0} ({1})", user.DisplayName, user.UserId);

			});

            if (app.Options.DatabaseUrl != null) app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);

            hasNewData = true;

        } //InitializeFirebase()

		// Track state changes of the auth object.
		void AuthStateChanged(object sender, System.EventArgs eventArgs) {

			if (auth.CurrentUser != user) {
				signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
				if (!signedIn && user != null) {
					Debug.Log("AuthStateChanged(): Signed out " + user.UserId);
				}
				user = auth.CurrentUser;
				if (signedIn) {
					Debug.Log("AuthStateChanged(): Signed in " + user.UserId);
				}
			}

		} // AuthStateChanged()

		void OnDestroy() {
			auth.StateChanged -= AuthStateChanged;
			auth = null;
		}

        // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        public void StoreRecording(string band_id, Quaternion q, string recordingName)
        {
            if (connected && signedIn)
            {
                Firebase.Auth.FirebaseUser user = auth.CurrentUser;
                if (user != null) { userId = user.UserId; } else userId = "DemoUser";
                
                // use this for adding reference movements: (permissions for refmoves allow all users to read + write!)
                refBandRecording = FirebaseDatabase.DefaultInstance.GetReference("/refmoves/" + recordingName + "/" + band_id);
                refBandRecording.Push().SetRawJsonValueAsync(JsonUtility.ToJson(q));

                Debug.Log("Sent data to record " + recordingName);
            }
            else
            {
                Debug.Log("FirebaseInit: trying to store data, but firebase not yet ready (connected=false or signedin=false). Dropping the package.");
            }
        }


        public void PushData(string band_id, Quaternion q)
        {

			Debug.Log("FirebaseInit: PushData()");

			if (connected && signedIn) {

				Firebase.Auth.FirebaseUser user = auth.CurrentUser;
				if (user != null) { userId = user.UserId; } else userId = "DemoUser";

                // permissions allow only to write anything under armbands/uid/* - no access to other users data!
                refBandCapturing = FirebaseDatabase.DefaultInstance.GetReference("/armbands/" + userId + "/" + session_id + "/" + band_id);
                refBandCapturing.Push().SetRawJsonValueAsync(JsonUtility.ToJson(q));
				Debug.Log("Pushing data done");

			} else {
				Debug.Log("FirebaseInit: trying to push data, but firebase not yet ready (connected=false or signedin=false). Dropping the package.");
			}

        } // PushData()



        public void RetrieveReferenceMovements() {

            StoredMovements.Clear();

            FirebaseDatabase.DefaultInstance.GetReference("refmoves").GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    Debug.Log("FirebaseInit: could not retrieve the reference movements");
                }
                else if (task.IsCompleted)
                {
                    refmoves = task.Result;
                    if (refmoves.HasChildren)
                    {
                        Debug.Log("# Children: " + refmoves.ChildrenCount);
                        
                        foreach (DataSnapshot ds in refmoves.Children)
                        {
                            StoredMovements.Add(ds.Key);
                            Debug.Log("Option Added: " + ds.Key);
                        }
                    }
                    else
                    {
                        Debug.Log("no movements found");
                    }

                    Debug.Log("FirebaseInit: successfully retrieved the reference movements.");
                    hasNewData = true;
                }
			});
            
		} // RetrieveReferenceMovements()


        public void LoadRefMovementToMemory(string movementName)
        {
            DateTime startTime = DateTime.Now;
            upperDataReceived.Clear();
            lowerDataReceived.Clear();

            FirebaseDatabase.DefaultInstance.GetReference("refmoves/" + movementName).GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.Log("FirebaseInit: could not load the selected movement");
                }
                else if (task.IsCompleted)
                {
                    currentMovement = task.Result;

                    DataSnapshot myLowerArm = currentMovement.Child("lowerArm");
                    Debug.Log("lower arm recording found, with " + myLowerArm.ChildrenCount.ToString() + " frames of data");

                    foreach (DataSnapshot ds1 in currentMovement.Child("lowerArm").Children)
                    {
                        float wL = float.Parse(ds1.Child("w").Value.ToString());
                        float xL = float.Parse(ds1.Child("x").Value.ToString());
                        float yL = float.Parse(ds1.Child("y").Value.ToString());
                        float zL = float.Parse(ds1.Child("z").Value.ToString());

                        Quaternion tempQuatL = new Quaternion(xL, yL, zL, wL);
                        lowerDataReceived.Add(tempQuatL);
                    }

                    DataSnapshot myUpperArm = currentMovement.Child("upperArm");
                    Debug.Log("upper arm recording found, with " + myUpperArm.ChildrenCount.ToString() + " frames of data");

                    foreach (DataSnapshot ds2 in currentMovement.Child("upperArm").Children)
                    {
                        float w = float.Parse(ds2.Child("w").Value.ToString());
                        float x = float.Parse(ds2.Child("x").Value.ToString());
                        float y = float.Parse(ds2.Child("y").Value.ToString());
                        float z = float.Parse(ds2.Child("z").Value.ToString());

                        Quaternion tempQuat = new Quaternion(x, y, z, w);
                        upperDataReceived.Add(tempQuat);
                    }

                    AnimationController aniCon = GameObject.Find("Anatomy/AnimatedShoulder").GetComponent<AnimationController>();
                    aniCon.upperData = upperDataReceived.ToArray();
                    aniCon.lowerData = lowerDataReceived.ToArray();

                    aniCon.SetArmPosition(aniCon.upperData[0], aniCon.lowerData[0]);

                    TimeSpan timeTaken = DateTime.Now - startTime;
                    Debug.Log("File Import completed in " + timeTaken.Milliseconds.ToString() + " milliseconds");

                    if (lowerDataReceived.Count > 0 && upperDataReceived.Count > 0)
                    {
                        PlayButton.SetActive(true);
                    }
                    else
                    {
                        PlayButton.SetActive(false);
                    }
                }
            });
        }


        private Quaternion stringToQuat(string inputString)
        {
            try
            {
                string[] components = inputString.Split(',');
                return new Quaternion((float.Parse(components[0])), (float.Parse(components[1])), (float.Parse(components[2])), float.Parse(components[3]));
            }
            catch
            {
                Debug.Log("error parsing string - identity quat given instead");
                return Quaternion.identity;
            }

        }


    } // Class firebaseinit

} // namespace CareBand
