using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;
using System.Threading.Tasks;
using System.Linq;


namespace CareBand
{
    public class blecontroller : MonoBehaviour
    {
        private firebaseinit myFB;

        private string _serviceUUID = "0000dfb0-0000-1000-8000-00805f9b34fb";               // bluno service
        private string _readCharacteristicUUID = "0000dfb1-0000-1000-8000-00805f9b34fb";    // bluno serial charac (read)
        private string _writeCharacteristicUUID = "0000dfb2-0000-1000-8000-00805f9b34fb";   // bluno command charac (write)

        public string upperArmDevice = "BlunoUpper";
        public string lowerArmDevice = "Bluno";

        private bool upperArmDeviceFound = false;
        private bool lowerArmDeviceFound = false;
        private bool isUpperConnected = false;
        private bool isLowerConnected = false;

        private string _connectedID = null;

        private Dictionary<string, string> _peripheralList;

        //public Text txtDebug;
        //public GameObject uiPanel;
        //public Text txtSend;
        //public Text txtReceive;
        //public Button btnSend;
        public Text txtDataUpper;
        public Text txtDataLower;

        private GameObject myUpperLight;
        private GameObject myLowerLight;

        public GameObject recText;
        public GameObject startRecButton;
        public GameObject stopRecButton;

        public string currentRecordingName = "testRecording";
        public bool IsRecording;

        private GameObject myShoulder;
        private GameObject myForearm;
        private Quaternion upperArmbandData;
        private Quaternion lowerArmbandData;

        private Quaternion rawQuatUpper;
        private Quaternion rawQuatLower;

        private int minLen = 10;

        private bool isUpperDataAvailable = false;
        private bool isLowerDataAvailable = false;

        public Dropdown listOfAnimations;
        public string currentMovement {get; set;}
        private bool selectorOpen = false;

        void Start()
        {
            
            IsRecording = false;

            RectTransform thisShape = listOfAnimations.gameObject.GetComponent<RectTransform>();
            thisShape.offsetMin = new Vector2(0f, -165f);

            myFB = GameObject.Find("start").GetComponent<firebaseinit>();

            BluetoothLEHardwareInterface.Initialize(false, true, () => { }, (error) => { Debug.Log("BLE init error"); });

            myShoulder = GameObject.Find("Anatomy/shoulder");
            myForearm = GameObject.Find("Anatomy/shoulder/r_shoulder/r_elbow");
            myUpperLight = GameObject.Find("BLE/connInd/upperLight");
            myLowerLight = GameObject.Find("BLE/connInd/lowerLight");

            startRecButton.SetActive(false);
            stopRecButton.SetActive(false);
            recText.gameObject.SetActive(false);

            listOfAnimations.onValueChanged.AddListener(delegate {
                OnMyValueChange(listOfAnimations);
            });
            Invoke("Scan", 1f);
        }



        void Update()
        {
            if (isUpperDataAvailable && isLowerDataAvailable)
            {
                txtDataUpper.text = upperArmbandData.ToString();
                txtDataLower.text = lowerArmbandData.ToString();

                myShoulder.transform.localRotation = upperArmbandData;
                myForearm.transform.rotation = lowerArmbandData;

                if (IsRecording)
                {
                    myFB.StoreRecording("upperArm", upperArmbandData, currentRecordingName);
                    myFB.StoreRecording("lowerArm", lowerArmbandData, currentRecordingName);
                }

                isUpperDataAvailable = false;
                isLowerDataAvailable = false;
            }


            if (myFB.connected && myFB.hasNewData)
            {
                myFB.hasNewData = false;
                UpdateDropdownAnimations();
                Debug.Log("Update received from firebase");
            }

        }


        public void StartDataStream()
        {
            IsRecording = true;
            startRecButton.gameObject.SetActive(false);
            stopRecButton.gameObject.SetActive(true);
            recText.gameObject.SetActive(true);
        }

        public void StopDataStream()
        {
            IsRecording = false;
            startRecButton.gameObject.SetActive(true);
            stopRecButton.gameObject.SetActive(false);
            recText.gameObject.SetActive(false);
        }


        public void OnMyValueChange(Dropdown dd)
        {
            currentMovement = dd.captionText.text;
            Debug.Log("currentMovement is " + currentMovement);
            myFB.LoadRefMovementToMemory(currentMovement);


        }

        public void UpdateDropdownAnimations()
        {
            if (myFB.StoredMovements != null)
            {
                listOfAnimations.options.Clear();
                foreach (string option in myFB.StoredMovements)
                {
                    listOfAnimations.options.Add(new Dropdown.OptionData(option));
                }
                listOfAnimations.value = 0;
                //listOfAnimations.options[0].text = "Choose from:";
                listOfAnimations.captionText.text = "Select...";
            }
            else
            {
                Debug.Log("coudln't get movements");
            }

            
        }

        private int openMenuLimit = -220;
        private int closeMenuLimit = 0;

        public void ToggleSelector()
        {
            // open dropdown
            if (!selectorOpen)
            {
                StartCoroutine(MoveMenu(closeMenuLimit, openMenuLimit));
                myFB.RetrieveReferenceMovements();
                startRecButton.SetActive(false);
            }

            //close dropdown
            else
            {
                StartCoroutine(MoveMenu(openMenuLimit, closeMenuLimit));
                listOfAnimations.value = 0;
                myFB.PlayButton.SetActive(false);
                startRecButton.SetActive(true);
            }
            selectorOpen = !selectorOpen;
            Debug.Log("sOpen: " + selectorOpen.ToString());
        }



        private IEnumerator MoveMenu(float startValue, float endValue)
        {
            RectTransform thisShape = listOfAnimations.gameObject.GetComponent<RectTransform>();
            
            float bottom = -165f;
            float speed = 500f;
            if (endValue < 0) speed *= -1f;

            float rate = 1.0f / (endValue - startValue) * speed;
            float t = 0.0f;
            while (t < 1.0)
            {
                t += Time.deltaTime * rate;

                float asd = Mathf.Lerp(startValue, endValue, t);
                thisShape.offsetMin = new Vector2(asd, bottom);
                yield return null;
            }
        }


        private void recieveNotification(string not)
        {
            Debug.Log("A device sent a notification: " + not);
        }



        private void receiveDataUpper(string s)
        {
            if (s.Length > minLen)
            {
                try
                {
                    string[] angles = s.Split(',');
                    rawQuatUpper = new Quaternion((float.Parse(angles[0]) - 1), (float.Parse(angles[1]) - 1), (float.Parse(angles[2]) - 1), float.Parse(angles[3]) - 1);
					upperArmbandData = BNOtoUnityOperation(rawQuatUpper);
                    isUpperDataAvailable = true;

                    if (IsRecording)
                    {
                        //myFB.PushData("upperArm", upperArmbandData);
                    }
                }
                catch
                {
					Debug.Log("blecontroller: error parsing string from upper device");
                }
            }
            else
            {
				Debug.Log("blecontroller: Incoming upper armband data too short (<" + minLen + ")");
            }
        }



        private void receiveDataLower(string s)
        {
            if (s.Length > minLen)
            {
                try
                {
                    string[] angles = s.Split(',');
                    rawQuatLower = new Quaternion((float.Parse(angles[0]) - 1), (float.Parse(angles[1]) - 1), (float.Parse(angles[2]) - 1), float.Parse(angles[3]) - 1);
                    lowerArmbandData = BNOtoUnityOperation(rawQuatLower) * Quaternion.Euler(-90, -90, 0);

                    if (IsRecording)
                    {
                        //myFB.PushData("lowerArm", lowerArmbandData);
                    }
                    isLowerDataAvailable = true;
                }
                catch
                {
                    Debug.Log("error parsing string from lower device");
                }
            }
            else
            {
                Debug.Log("Incoming upper armband data too short (<" + minLen + ")");
            }
        }


        private Quaternion BNOtoUnityOperation(Quaternion bnoQuat)
        {
            // switch to left-handed coordinates
            Quaternion q_lefthanded = new Quaternion(-bnoQuat[1], -bnoQuat[3], -bnoQuat[2], bnoQuat[0]);

            // now working only in unity quats
            Vector3 v = q_lefthanded.eulerAngles;
            Quaternion unityQuat = Quaternion.AngleAxis(v.y, Vector3.up) * Quaternion.AngleAxis(v.x, Vector3.right) * Quaternion.AngleAxis(v.z, Vector3.forward);

            // mirror y quat component
            unityQuat.y *= -1;
            unityQuat.w *= -1;

            return unityQuat;
            //return q_lefthanded;
        }


        void sendBytesBluetooth(byte[] data)
        {
            BluetoothLEHardwareInterface.Log(string.Format("data length: {0} uuid {1}", data.Length.ToString(), FullUUID(_writeCharacteristicUUID)));
            BluetoothLEHardwareInterface.WriteCharacteristic(_connectedID, FullUUID(_serviceUUID), FullUUID(_writeCharacteristicUUID),
                                                              data, data.Length, true, (characteristicUUID) =>
                                                              {
                                                                  BluetoothLEHardwareInterface.Log("Write succeeded");
                                                              }
                                                              );
        }

        private void Scan()
        {

            //txtDebug.text = ("Scanning for devices... \n");
			Debug.Log("BLEcontroller: Scanning for devices...\n");

            try
            {
                BluetoothLEHardwareInterface.ScanForPeripheralsWithServices(null,
                    (address, name) =>
                    {
                        AddPeripheral(name, address);
                    },
                    null // breaks on ios	( address, name, rssi, info ) => {Debug.Log("BLEcontroller: found device again " + name);}

                    );
            }
            catch
            {
				Debug.Log("BLEcontroller: Bluetooth could not initialise");
                myUpperLight.GetComponent<Renderer>().material.color = Color.blue;
                myLowerLight.GetComponent<Renderer>().material.color = Color.blue;
            }
        }


        void AddPeripheral(string name, string address)
        {

            if (_peripheralList == null)
            {
                _peripheralList = new Dictionary<string, string>();
            }

            if (!_peripheralList.ContainsKey(address))
            {
                _peripheralList[address] = name;

				Debug.Log("BLEcontroller: AddPeripheral: checking whether to connect to " + name + " (" + address + ")");

                if (name.Trim().ToLower() == upperArmDevice.Trim().ToLower() && !upperArmDeviceFound)
                {
                    //txtDebug.text += "Upper arm device found \n";
                    connectBluetoothUpper(address);
                    upperArmDeviceFound = true;
                }

                else if (name.Trim().ToLower() == lowerArmDevice.Trim().ToLower() && !lowerArmDeviceFound)
                {
                    //txtDebug.text += "Lower arm device found \n";
                    connectBluetoothLower(address);
                    lowerArmDeviceFound = true;
                }
            }
            else
            {
                Debug.Log("Address not found");
            }

        } // addPeripheral();


        private void connectBluetoothUpper(string addr)
        {
            BluetoothLEHardwareInterface.ConnectToPeripheral(
                addr, (address) => { }, (address, serviceUUID) => { },

                (address, serviceUUID, characteristicUUID) =>
                {

                    if (IsEqual(serviceUUID, _serviceUUID))
                    {
                        _connectedID = address;
                        isUpperConnected = true;

                        Color myColour = new Color();
                        ColorUtility.TryParseHtmlString("#40FF40FF", out myColour);
                        myUpperLight.GetComponent<Renderer>().material.color = myColour;

                        notifyCharacteristicUpper();

                        if (isLowerConnected)
                        {
                            BluetoothLEHardwareInterface.StopScan();
                            //txtDebug.text += "Scanning has ceased \n";
                            startRecButton.SetActive(true);
                        }
                    }
                },

                (address) =>
                {

                // this will get called when the device disconnects
                // be aware that this will also get called when the disconnect
                // is called above. both methods get call for the same action
                // this is for backwards compatibility
                myUpperLight.GetComponent<Renderer>().material.color = Color.red;
                    isUpperConnected = false;
                });
        }


        private void connectBluetoothLower(string addr2)
        {
            BluetoothLEHardwareInterface.ConnectToPeripheral(
                addr2, (address2) => { },
                (address2, serviceUUID2) => { },

                (address2, serviceUUID2, characteristicUUID2) =>
                {

                    if (IsEqual(serviceUUID2, _serviceUUID))
                    {
                        _connectedID = address2;
                        isLowerConnected = true;

                        Color myColour = new Color();
                        ColorUtility.TryParseHtmlString("#40FF40FF", out myColour);
                        myLowerLight.GetComponent<Renderer>().material.color = myColour;

                        notifyCharacteristicLower();

                        if (isUpperConnected)
                        {
                            BluetoothLEHardwareInterface.StopScan();
                            //txtDebug.text += "Scanning has ceased \n";
                            startRecButton.SetActive(true);
                        }
                    }
                },

                (address) =>
                {
                    myLowerLight.GetComponent<Renderer>().material.color = Color.red;
                    isLowerConnected = false;
                });
        }


        private void notifyCharacteristicUpper()
        {
            BluetoothLEHardwareInterface.SubscribeCharacteristic(
            _connectedID, _serviceUUID, _readCharacteristicUUID,

            (notification) =>
            {
                recieveNotification(notification);
            },

            (characteristic, data) =>
            {
                if (data.Length != 0)
                {
                    var length = data.TakeWhile(b => b != 0).Count();
                    var s = Encoding.UTF8.GetString(data, 0, length);

                receiveDataUpper(s);
                }
            });
        }

        private void notifyCharacteristicLower()
        {
            BluetoothLEHardwareInterface.SubscribeCharacteristic(
            _connectedID, _serviceUUID, _readCharacteristicUUID,

            (notification) =>
            {
                recieveNotification(notification);
            },

            (characteristic, data) =>
            {
                if (data.Length != 0)
                {
                    var length = data.TakeWhile(b => b != 0).Count();
                    var s = Encoding.UTF8.GetString(data, 0, length);

                receiveDataLower(s);
                }
            });
        }


        void sendData()
        {
            string s = "";// txtSend.text.ToString();

            if (s.Length > 0)
            {
                byte[] bytes = ASCIIEncoding.UTF8.GetBytes(s);
                if (bytes.Length > 0)
                {
                    sendBytesBluetooth(bytes);
                }
            }
        }

        // -------------------------------------------------------
        // some helper functions for handling connection strings
        // -------------------------------------------------------
        public static string FullUUID(string uuid)
        {
            if (uuid.Length == 4)
                return "0000" + uuid + "-0000-1000-8000-00805f9b34fb";
            return uuid;
        }

        bool IsEqual(string uuid1, string uuid2)
        {
            if (uuid1.Length == 4)
            {
                uuid1 = FullUUID(uuid1);
            }
            if (uuid2.Length == 4)
            {
                uuid2 = FullUUID(uuid2);
            }
            return (uuid1.ToUpper().CompareTo(uuid2.ToUpper()) == 0);
        }

    }

}

