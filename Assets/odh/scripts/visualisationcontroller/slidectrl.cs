using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class slidectrl : MonoBehaviour {

    private Animator anim;
    public Slider slider;

    void Start () {

        anim = GetComponent<Animator> ();
        anim.speed = 0;

    }

    void Update()
    {
        //Debug.Log(slider.normalizedValue);
        anim.Play("flex", -1, slider.normalizedValue);
    }

}
