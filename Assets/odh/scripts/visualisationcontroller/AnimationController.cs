﻿using UnityEngine;
using UnityEngine.UI;

namespace CareBand
{
    public class AnimationController : MonoBehaviour
    {
        public Quaternion[] upperData { get; set; }
        public Quaternion[] lowerData { get; set; }

        public Text animationPosUpper;
        public Text animationPosLower;

        private bool isPlaying = false;
        private int currentFrame = 0;
        private GameObject myUpperArm;
        private GameObject myLowerArm;

        void Start()
        {
            myUpperArm = this.gameObject;
            myLowerArm = GameObject.Find("Anatomy/AnimatedShoulder/r_shoulder/r_elbow").gameObject;
        }

        void Update()
        {
            if (isPlaying)
            {

                if (currentFrame < upperData.Length)
                {
                    SetArmPosition(upperData[currentFrame], lowerData[currentFrame]);
                    animationPosUpper.text = upperData[currentFrame].ToString();
                    animationPosLower.text = lowerData[currentFrame].ToString();
                    currentFrame++;
                }
                else
                {
                    isPlaying = false;
                }
            }
        }

        public void SetArmPosition(Quaternion upperQuat, Quaternion lowerQuat)
        {
            myUpperArm.transform.localRotation = upperQuat;
            myLowerArm.transform.rotation = lowerQuat;
        }

        public void PlayRecording()
        {
            currentFrame = 0;
            if (upperData.Length > 0 && lowerData.Length > 0)
            {
                isPlaying = true;
            }
            else
            {
                Debug.Log("one of the data areays is empty");
            }
        }

    }
}
